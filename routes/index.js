var express = require("express");
var router = express.Router();

const Queue = require("bull");
var ping = require("ping");

//var host = "localhost";

// Initiating the Queue
const pingQueue = new Queue("ping", {
  hostId: "Bull",
  type: "bull",
  redis: {
    host: "127.0.0.1",
    port: 6379,
    //password: "root",
  },
});
const data = {
  host: "127.0.0.1",
};

const options = {
  //delay: 60000, // 1 min in ms
  delay: 1000, // 1 s in ms
  attempts: 1,
};

router.get("/", function (req, res, next) {
  res.render("index", { title: "Welcome to Bull" });
});

router.get("/ping", function (req, res) {
  res.render("index", { title: "Adding a Job to the Queue.." });
  // Adding a Job to the Queue
  pingQueue.add(data, options);
});

// Consumer
pingQueue.process(async (job) => {
  // ping(job.data.host);

  // Wait 5sec
  await new Promise((res) => setTimeout(res, 5000));

  // Randomly succeeds or fails the job to put some jobs in completed and some in failed.
  if (Math.random() > 0.5) {
    throw new Error("fake error");
  }
});

// function ping(host) {
//   ping.promise.probe(host).then(function (res) {
//     console.log(res);
//   });
//   return res;
// }

module.exports = router;
