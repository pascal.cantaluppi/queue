var Queue = require("bull");

var testQueue = new Queue("test");

testQueue.process(function (job, done) {
  console.log("Received message", job.data.msg);
  done();
});
