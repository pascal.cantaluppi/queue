## Overview

This is a simple demonstration of how to run Arena and connect it to [Bull Queue](https://github.com/OptimalBits/bull).

## Requirements

- No other services running on ports 4735 or 4736
- Firewall open for port 4735 and 4736

## Install

`npm i`
`npm i pm2@latest -g`

## Running

`pm2 start ui.js`

Then open http://localhost:3000 in your browser.
